<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageNotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $position;
    public $figura;
    public $round;
    public $reset;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($position,$figura,$round,$reset)
    {
        //posicion del campo
        $this->position = $position;
        //el valor de la figura X o O
        $this->figura = $figura;
        //el turno
        $this->round = (int)$round;
        //para resetear la partida para ambos jugadores 
        $this->reset = (int)$reset;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('notification');
    }

    public function broadcastAs()
    {
        //nombre del evento
        return 'MessageNotification';
    }
}
