<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Partida;
use App\Events\MessageNotification;

class PartidaController extends Controller
{
    function createRound(Request $request) {
        // genera el codigo random de nueva partida
        $random_code = Str::random(6);
        // genera el codigo random del jugador 1
        $random_player_1 = Str::random(6);
       // genera el codigo random del jugaodor 2
        $random_player_2 = Str::random(6);
        $partida = new Partida();
        $partida->code_random= $random_code;
        $partida->jugador1_id= $random_player_1;

        // recibe nombre del usuario si está vacio lo pone por defecto Jugador 1
        $jugador_name=($request->name!='') ? $request->name: 'Jugador 1';
        $partida->jugador1_name=  $jugador_name;
        $partida->jugador2_id= $random_player_2;
        //guarda en base de datos
        $partida->save();
        // armamos el json de los jugadores 
        //como el jugador uno crea la partida entonce owner es true, su figura es X y el primer turno es para el jugador 1
    
        $data = [
            'random_code' => $random_code,
            'player_1' => ['id' => $random_player_1,'name'=> $jugador_name, 'owner' => true,'figura'=>'X'],
            'player_2' => ['id' => $random_player_2,'name'=>'Jugador 2', 'owner' => false,'figura'=>'O'],
            'round' => 1,
            
        ];
       
        //retornamos la vista  y le enviamos los datos
        return view('partida',compact('data', 'random_code','jugador_name'));
    }


    function joinRound(Request $request) {
        // mensaje error
        $error='El id de partida no existe o ya ha sido iniciada';
        //busca un id de partida
        $partida =  Partida::where('code_random',$request->id)
        ->where('init',0)->first();
        //el normbre del jugador 2 queda por defecto
        $jugador_name='Jugador 2';

        //valida si la consulta trae datos 
        if( $partida!=null){
            $random_code = $partida->code_random;

             // armamos el json de los jugadores 
            //como el jugador uno crea la partida entonce owner es true, su figura es X y el primer turno es para el jugador 1
            $data = [
                'random_code' =>  $partida->code_random,
                'player_1' => ['id' =>  $partida->jugador1_id,'name'=> $partida->jugador1_name, 'owner' => false,'figura'=>'X'],
                'player_2' => ['id' =>   $partida->jugador2_id,'name'=> $jugador_name, 'owner' => true,'figura'=>'O'],
                'round' => 1
            ];
            //modifica en base de datos la columna init para decir que ya se inicio la partida
            $partida->init=1;
            $partida->save();
            //retornamos la vista  y le enviamos los datos
            return view('partida',compact('data', 'random_code','jugador_name'));
        }
       
        //retornamos la vista  y le enviamos los datos con el 
        return view('welcome',compact('error'));
    }

    function trigger_websocke(Request $request){
        //recibimos la posicion 
        $position = $request->get('position');
        //recibimos la figura X o O
        $figura = $request->get('figura');
        //recibimos el turno 1: jugador 1 y 2: jugador 2
        $round = $request->get('round');
        //parametro que nos sirve para reiniciar la partida
        $reset = ($request->get('reset')==1) ? 1: 0;
        // invocamos evento MessageNotification y  le mandamos los parametros al constructor
        $event=broadcast(new MessageNotification($position,$figura,$round,$reset));
        //retornamos un 200 para decir que entro al evento correctamente
        if($event){
            return response(200);
        }
        return response(400);
        
       
    }

}
