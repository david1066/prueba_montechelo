<?php

use Illuminate\Database\Seeder;

class ElementosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('elementos')->insert(array(
            'nombre' => 'X'));
        \DB::table('elementos')->insert(array(
            'nombre' => 'O'));
    }
}
