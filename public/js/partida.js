
// inicia escuhando los sockets de la partida por el canal notification y el evento MessageNotification
window.Echo.channel('notification')
    .listen('.MessageNotification', (e) => {

        let tabs = document.getElementsByClassName("board-element");
        //marca la casilla con X o O
        tabs[e.position - 1].innerHTML = e.figura;
        let game_data = JSON.parse(localStorage.getItem('game_data'));
        game_data.round = e.round;
        localStorage.setItem('game_data', JSON.stringify(game_data));

        let finalizado = validate();

        //si finalizado es igual a true el tablero queda bloqueado para ambos contricantes ç
        //y almacenamos en el local storage de ambos jugadores
        if (finalizado) {


            game_data.round = 0;
            localStorage.setItem('game_data', JSON.stringify(game_data));
            document.getElementById("reset").classList.remove("hide");
        }
        let reset = e.reset;
        //Sí reset es igual a uno esconde el boton reiniciar partida
        if (reset == 1) {
            game_data.round = e.round;
            localStorage.setItem('game_data', JSON.stringify(game_data));

            document.getElementById("reset").classList.add("hide");
        }
        let message = document.getElementById("message");
        if(game_data.round==1){
            message.innerHTML='Turno de '+game_data.player_1.name;
        }else if(game_data.round==2){
            message.innerHTML='Turno de jugador 2';
        }
    })


//inicia la partica 
init = () => {

    let tabs = document.getElementsByClassName("board-element");

    for (let i = 0; i < tabs.length; i++) {


        tabs[i].addEventListener('click', () => {

            let game_data = JSON.parse(localStorage.getItem('game_data'));
            let figura = game_data.player_2.figura;
            let round = game_data.round;
            if (game_data.player_1.owner) {
                figura = game_data.player_1.figura;
            }

            //cambia el turno de cada jugador 
            if (round == 1) {
                game_data.round = 2;
            } else {
                game_data.round = 1;
            }

            //validar a que jugador le toca jugar 
            if ((game_data.player_1.owner && round == 1) || (game_data.player_2.owner && round == 2)) {
                // enviamos peticion ajax al socket
                $.post('/trigger-websocket', {
                    position: i + 1,
                    figura: figura,
                    round: game_data.round
                }).fail(() => {
                        alert('Error sending event.');
                });
            }



        })




    }
}
//valida si hay un ganador o es un empate
validate = () => {
    let tabs = document.getElementsByClassName("board-element");
    let message = document.getElementById("message");

    
    //validar por fila uno  
    if (tabs[0].innerHTML == tabs[1].innerHTML && tabs[0].innerHTML == tabs[2].innerHTML) {
        if (tabs[0].innerHTML == 'X' || tabs[0].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[0].innerHTML);
            return true;
        }

    }
    //validar por fila dos  
    if (tabs[3].innerHTML == tabs[4].innerHTML && tabs[3].innerHTML == tabs[5].innerHTML) {
        if (tabs[3].innerHTML == 'X' || tabs[3].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[3].innerHTML);
            return true;
        }
    }
    //validar por fila tres 
    if (tabs[6].innerHTML == tabs[7].innerHTML && tabs[6].innerHTML == tabs[8].innerHTML) {
        if (tabs[6].innerHTML == 'X' || tabs[6].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[6].innerHTML);
            return true;
        }
    }
    //validar por columna uno  
    if (tabs[0].innerHTML == tabs[3].innerHTML && tabs[0].innerHTML == tabs[6].innerHTML) {
        if (tabs[0].innerHTML == 'X' || tabs[0].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[0].innerHTML);
            return true;
        }
    }
    //validar por columna dos   
    if (tabs[1].innerHTML == tabs[4].innerHTML && tabs[1].innerHTML == tabs[7].innerHTML) {
        if (tabs[1].innerHTML == 'X' || tabs[1].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[1].innerHTML);
            return true;
        }
    }
    //validar por columna tres  
    if (tabs[2].innerHTML == tabs[5].innerHTML && tabs[2].innerHTML == tabs[8].innerHTML) {
        if (tabs[2].innerHTML == 'X' || tabs[2].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[2].innerHTML);
            return true;
        }
    }
    //validar por diagonal uno  
    if (tabs[0].innerHTML == tabs[4].innerHTML && tabs[0].innerHTML == tabs[8].innerHTML) {
        if (tabs[0].innerHTML == 'X' || tabs[0].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[0].innerHTML);
            return true;
        }
    }

    //validar por diagonal dos 
    if (tabs[6].innerHTML == tabs[4].innerHTML && tabs[6].innerHTML == tabs[2].innerHTML) {
        if (tabs[6].innerHTML == 'X' || tabs[6].innerHTML == 'O') {
            message.innerHTML=jugador(tabs[6].innerHTML);
            return true;
        }
    }

    //validar cuando todos los campos esten llenos o hay un empate

    if (tabs[0].innerHTML != '' && tabs[1].innerHTML != '' && tabs[2].innerHTML != ''
        && tabs[3].innerHTML != '' && tabs[4].innerHTML != '' && tabs[5].innerHTML != ''
        && tabs[6].innerHTML != '' && tabs[7].innerHTML != '' && tabs[8].innerHTML != '') {

         message.innerHTML='Es un empate';
        return true;
    }


    return false;


}
//obtener nombre del jugador
jugador = (figura) => {
    let game_data = JSON.parse(localStorage.getItem('game_data'));
    let jugador = game_data.player_1.name+ ' Ganador';
    if (figura == 'O') {
        jugador = 'Jugador 2 Ganador';
    }


    return jugador;
}
//reinicia la partida

var turno = 1;
reset = () => {
    if (turno == 1) {
        turno = 2;
    } else {
        turno = 1;
    }


    let tabs = document.getElementsByClassName("board-element");
    for (let i = 0; i < tabs.length; i++) {
        $.post('/trigger-websocket', {
            position: i + 1,
            figura: '',
            round: turno,
            reset: 1,
        })
            .fail(() => {
                alert('Error sending event.');
            });
    }

}



init()

