<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tablero</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
  <style>
    
     
   .container-board {
      max-width: 700px;
      -webkit-touch-callout: none;
         -webkit-user-select: none;
         -khtml-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         user-select: none;
   }

   .board-element {
      height: 200px;
 
      background-color: #FAFAFA;
      margin: 10px;
      z-index: 2;
      border-radius: 10px;
      border: solid black 1px;
      font-size: 8rem;
      display: flex;
      justify-content: center;
      

   }
   .hide{
      visibility: hidden;
   }

</style>
       
  
<div class="container text-center">
 <h1 class="text-info mb-4">Tic Tac Toe</h1>
 <h2>Codigo partida : <small>{{$random_code}}</small></h2>
 <h2>Nombre Jugador: <small> {{$jugador_name}}</small></h2>
 <div class="alert alert-info" role="alert" id="message"> </div>
   <div class="form-group text-center">
     <div class="container container-board">
         <div class="row mb-1">
            <div class="col-4">
               <div class="board-element"></div>
            </div>
            <div class="col-4">
               <div class="board-element"></div>
            </div>
            <div class="col-4">
               <div class="board-element"></div>
            </div>
         </div>

         <div class="row mb-1">
            <div class="col-4">
               <div class="board-element"></div>
            </div>
            <div class="col-4">
               <div class="board-element"></div>
            </div>
            <div class="col-4">
               <div class="board-element"></div>
            </div>
         </div>

         <div class="row mb-1">
            <div class="col-4">
                  <div class="board-element"></div>
            </div>
            <div class="col-4">
               <div class="board-element"></div>
            </div>
            <div class="col-4">
               <div class="board-element"></div>
            </div>
         </div>

     </div>
     <a  class="btn btn-info hide" id="reset" onclick="reset()">Reiniciar Partida</a>
   </div>
   <div id="div-data"></div>
</div>
<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/partida.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script>
   const user = JSON.stringify(@json($data))   
   let message = document.getElementById("message");
   let game_data=@json($data);
   message.innerHTML='Turno de '+game_data.player_1.name;
   
   localStorage.setItem('game_data', user)
</script>
</body>
</html>