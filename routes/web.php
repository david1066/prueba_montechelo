<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//ruta por defecto
Route::get('/', function () {
    return view('welcome');
});
// rutas para crear partida
Route::post('/create-round', 'PartidaController@createRound');

//ruta para unirse a la partida
Route::post('/join-round', 'PartidaController@joinRound');


//ruta del canal 
Route::post('/trigger-websocket', 'PartidaController@trigger_websocke');